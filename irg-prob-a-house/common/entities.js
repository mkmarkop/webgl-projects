/**
 * Class representing an event that can be subscribed to.
 */
function AppEvent() {
  this._listeners = [];
}

/**
 * Subscribes a function to the event. Function will be evoked
 * when the event fires, and event data will be passed as an argument.
 * @param {Function} listener Listener function.
 */
AppEvent.prototype.subscribe = function(listener) {
  this._listeners.push(listener);
};

/**
 * Fires event and notifies all subscribed listeners about it,
 * also informing them about the event through data passed as an argument.
 * @param {Object} data Event data.
 */
AppEvent.prototype.notify = function(data) {
  for (let i = 0, n = this._listeners.length; i < n; i++) {
    this._listeners[i](data);
  }
};


/**
 * Class representing a grid that consists of square cells.
 * @param {number} cellSize Size of a single grid cell, representing both width and height.
 */
function Grid(cellSize) {
  this.cellSize = [cellSize, cellSize];
  this.offset = [0, 0];
}

/**
 * Updates grid offset so that cell begins at the screen center.
 * @param {Object} screen Screen object containing center position data.
 * @param {Object} screen.center Screen center position.
 * @param {number} screen.center.x Screen center x-coordinate.
 * @param {number} screen.center.y Screen center y-coordinate.
 */
Grid.prototype.updateOffset = function(screen) {
  this.offset[0] = screen.center.x % this.cellSize[0];
  this.offset[1] = screen.center.y % this.cellSize[1];
};

/**
 * Returns the size of a single grid cell.
 * @returns {Object} Cell size.
 */
Grid.prototype.size = function() {
  return {
    width: this.cellSize[0],
    height: this.cellSize[1],
  };
};

/**
 * Creates a new GL grid vertex buffer.
 * @param {WebGLRenderingContext} gl WebGL canvas context.
 * @returns {Object} Object that contains grid vertex buffer.
 */
Grid.createBuffers = function(gl) {
  return {
    vertex: {
      buffer: Grid.createVertexBuffer(gl),
      size: 2,
      type: gl.FLOAT,
      normalize: false,
      stride: 0,
      offset: 0,
    },
  };
};

Grid.createVertexBuffer = function(gl) {
  const gridVertices = [
    1.0, 1.0,
    -1.0, 1.0,
    1.0, -1.0,
    -1.0, -1.0,
  ];

  const gridBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, gridBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(gridVertices),
    gl.STATIC_DRAW);

  return gridBuffer;
};

/**
 * Creates a new house whose size is determined by grid cell size.
 * @param {*} cellSize Factory by which all house vertices will be scaled.
 */
function House(cellSize) {
  this.matrixSeq = new MatrixSequence();
  this.matrixSeq.setCurrentMatrix(mat3.fromScaling(
    cellSize, cellSize
  ));
  this.matrixSeq.save();
}

/**
 * Returns the transformation of the house which transforms
 * its local coordinates to world coordinates.
 * @returns {number[]} Model matrix of the house.
 */
House.prototype.matrix = function() {
  return this.matrixSeq.getTransform();
};

/**
 * Returns the current position of the house origin.
 * @returns {Object} 2D vector position.
 */
House.prototype.position = function() {
  const matrix = this.matrix();
  return {
    x: matrix[6],
    y: matrix[7],
  };
};

/**
 * Calculates Euclidean calculateDistance between the origin of the house
 * and the given world point.
 * @param {Object} pos 2D point in world coordinates.
 * @param {number} pos.x X-coordinate of the point.
 * @param {number} pos.y Y-coordinate of the point.
 * @returns {Object} 2D vector calculateDistance.
 */
House.prototype.distanceVector = function(pos) {
  const from = this.position();
  return {
    x: from.x - pos.x,
    y: from.y - pos.y,
  };
};

/**
 * Outmost vertices of the house.
 */
House.boundingPoints = [
  [0, 0],
  [2, 0],
  [2, 2],
  [1, 3],
  [0, 2],
];

/**
 * Checks if the given point is inside the house.
 * @param {Object} point 2D point in world coordinates.
 * @param {number} point.x X-coordinate of the point.
 * @param {number} point.y Y-coordinate of the point.
 * @returns {boolean} Returns true if this house contains the given point.
 */
House.prototype.contains = function(point) {
  let odd = false;
  const points = House.boundingPoints;
  const nPoints = points.length;
  const matrix = this.matrix();

  for (let i = 0, j = nPoints - 1, n = nPoints; i < n; j = i++) {
    const from = mat3.multiplyVector(matrix, [
      points[i][0],
      points[i][1],
      1,
    ]);
    const to = mat3.multiplyVector(matrix, [
      points[j][0],
      points[j][1],
      1,
    ]);

    // Is the point's Y coordinate within the edge's Y-range?
    if ((from[1] > point.y) !== (to[1] > point.y)) {
      // Is the point in the half-plane to the left of the extended edge?
      if ((point.y - from[1]) * (to[0] - from[0]) / (to[1] - from[1]) + from[0]
        > point.x) {
        odd = !odd;
      }
    }
  }

  return odd;
};

/**
 * Creates GL vertex and color buffers for a house object.
 * @param {WebGLRenderingContext} gl WebGL canvas context.
 * @param {Object} houseColors Object defining colors for every part of the house.
 * @param {number[]} houseColors.wall Color of house wall.
 * @param {number[]} houseColors.doors Color of house doors.
 * @param {number[]} houseColors.windows Color of house windows.
 * @param {number[]} houseColors.roof Color of house roof.
 * @returns {Object} Object containing house vertex and color buffers.
 */
House.createBuffers = function(gl, houseColors) {
  return {
    vertex: {
      buffer: House.createVertexBuffer(gl),
      size: 2,
      type: gl.FLOAT,
      normalize: false,
      stride: 0,
      offset: 0,
    },

    color: {
      buffer: House.createColorBuffer(gl, houseColors),
      size: 3,
      type: gl.FLOAT,
      normalize: false,
      stride: 0,
      offset: 0,
    },
  };
};

House.uniqueVertices = [
  [0, 0], [0.25, 0], [0.75, 0],
  [1.25, 0], [1.75, 0], [2, 0],
  [0.25, 1], [0.75, 1], [1.25, 1.25],
  [1.75, 1.25], [1.25, 1.75], [1.75, 1.75],
  [0, 2], [0.25, 2], [0.75, 2], [1, 2],
  [1.25, 2], [1.75, 2], [2, 2], [1, 3],
];

House.houseVertices = function() {
  const uniqueVertices = House.uniqueVertices;
  return [
    uniqueVertices[0], uniqueVertices[1], uniqueVertices[13],
    uniqueVertices[0], uniqueVertices[13], uniqueVertices[12],
    uniqueVertices[6], uniqueVertices[7], uniqueVertices[14],
    uniqueVertices[6], uniqueVertices[14], uniqueVertices[13],
    uniqueVertices[2], uniqueVertices[3], uniqueVertices[16],
    uniqueVertices[2], uniqueVertices[16], uniqueVertices[14],
    uniqueVertices[3], uniqueVertices[4], uniqueVertices[9],
    uniqueVertices[3], uniqueVertices[9], uniqueVertices[8],
    uniqueVertices[10], uniqueVertices[11], uniqueVertices[17],
    uniqueVertices[10], uniqueVertices[17], uniqueVertices[16],
    uniqueVertices[4], uniqueVertices[5], uniqueVertices[18],
    uniqueVertices[4], uniqueVertices[18], uniqueVertices[17]
  ];
};

House.doorVertices = function() {
  const uniqueVertices = House.uniqueVertices;
  return [
    uniqueVertices[1], uniqueVertices[2], uniqueVertices[7],
    uniqueVertices[1], uniqueVertices[7], uniqueVertices[6]
  ];
};

House.windowVertices = function() {
  const uniqueVertices = House.uniqueVertices;
  return [
    uniqueVertices[8], uniqueVertices[9], uniqueVertices[11],
    uniqueVertices[8], uniqueVertices[11], uniqueVertices[10]
  ];
};

House.roofVertices = function() {
  const uniqueVertices = House.uniqueVertices;
  return [
    uniqueVertices[12], uniqueVertices[15], uniqueVertices[19],
    uniqueVertices[15], uniqueVertices[18], uniqueVertices[19]
  ];
};

House.createColoredVertices = function() {
  return House.houseVertices()
    .concat(House.doorVertices())
    .concat(House.windowVertices())
    .concat(House.roofVertices());
};

House.createVertexBuffer = function (gl) {
  const coloredVertices = House.createColoredVertices();

  let vertices = [];
  for (let i = 0, n = coloredVertices.length; i < n; i++) {
    const vertex = coloredVertices[i];
    vertices = vertices.concat(vertex);
  }

  const vertexBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);

  return vertexBuffer;
};

House.createColorBuffer = function(gl, houseColors) {
  const triangleColors = [
    houseColors.wall,
    houseColors.wall,
    houseColors.wall,
    houseColors.wall,
    houseColors.wall,
    houseColors.wall,
    houseColors.wall,
    houseColors.wall,
    houseColors.wall,
    houseColors.wall,
    houseColors.wall,
    houseColors.wall,
    houseColors.doors,
    houseColors.doors,
    houseColors.windows,
    houseColors.windows,
    houseColors.roof,
    houseColors.roof,
  ];
  let colors = [];
  for (let i = 0, n = triangleColors.length; i < n; i++) {
    const triangleColor = triangleColors[i];
    colors = colors.concat(triangleColor, triangleColor, triangleColor);
  }
  const colorBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);

  return colorBuffer;
};