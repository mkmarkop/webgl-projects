'use strict';

/**
 * Module for vector operations.
 * @namespace
 */
const vec2 = {
  /**
   * Computes Euclidean calculateDistance between two 2D points given in homogenous coordinates.
   * @param {number[]} a 2D point in homogenous coordinates.
   * @param {number[]} b 2D point in homogenous coordinates.
   * @returns {number} Euclidean calculateDistance of two points.
   */
  calculateDistance: function(a, b) {
    return Math.sqrt(
      Math.pow((a[0] - b[0]), 2) +
      Math.pow((a[1] - b[1]), 2)
    );
  },
};

/**
 * Module for matrix operations. All matrices are represented by their columns,
 * therefore matrix multiplication order is: v' = Mv.
 * @namespace
 */
const mat3 = {
  /**
   * Multiplies 3D vector by a 3x3 matrix.
   * @param {number[]} a Column-major 3x3 transformation matrix.
   * @param {number[]} b A 3D vector.
   * @returns {number[]} Transformed 3D vector.
   */
  multiplyVector: function(a, b) {
    if (b.length === 2) {
      b.push(1);
    }
    // first column of matrix a
    const a00 = a[0 * 3 + 0];
    const a10 = a[0 * 3 + 1];
    const a20 = a[0 * 3 + 2];
    // second column of matrix a
    const a01 = a[1 * 3 + 0];
    const a11 = a[1 * 3 + 1];
    const a21 = a[1 * 3 + 2];
    // third column of matrix a
    const a02 = a[2 * 3 + 0];
    const a12 = a[2 * 3 + 1];
    const a22 = a[2 * 3 + 2];

    return [
      b[0] * a00 + b[1] * a01 + b[2] * a02,
      b[0] * a10 + b[1] * a11 + b[2] * a12,
      b[0] * a20 + b[1] * a21 + b[2] * a22,
    ];
  },

  /**
   * Multiplies two 3x3 matrices.
   * @param {number[]} a Column-major 3x3 matrix.
   * @param {number[]} b Column-major 3x3 matrix.
   * @returns {number[]} Matrix a multiplied by matrix b.
   */
  multiply: function(a, b) {
    // first column of matrix a
    const a00 = a[0 * 3 + 0];
    const a10 = a[0 * 3 + 1];
    const a20 = a[0 * 3 + 2];
    // second column of matrix a
    const a01 = a[1 * 3 + 0];
    const a11 = a[1 * 3 + 1];
    const a21 = a[1 * 3 + 2];
    // third column of matrix a
    const a02 = a[2 * 3 + 0];
    const a12 = a[2 * 3 + 1];
    const a22 = a[2 * 3 + 2];

    // first column of matrix b
    const b00 = b[0 * 3 + 0];
    const b10 = b[0 * 3 + 1];
    const b20 = b[0 * 3 + 2];
    // second column of matrix b
    const b01 = b[1 * 3 + 0];
    const b11 = b[1 * 3 + 1];
    const b21 = b[1 * 3 + 2];
    // third column of matrix b
    const b02 = b[2 * 3 + 0];
    const b12 = b[2 * 3 + 1];
    const b22 = b[2 * 3 + 2];

    const c00 = a00 * b00 + a01 * b10 + a02 * b20;
    const c01 = a00 * b01 + a01 * b11 + a02 * b21;
    const c02 = a00 * b02 + a01 * b12 + a02 * b22;

    const c10 = a10 * b00 + a11 * b10 + a12 * b20;
    const c11 = a10 * b01 + a11 * b11 + a12 * b21;
    const c12 = a10 * b02 + a11 * b12 + a12 * b22;

    const c20 = a20 * b00 + a21 * b10 + a22 * b20;
    const c21 = a20 * b01 + a21 * b11 + a22 * b21;
    const c22 = a20 * b02 + a21 * b12 + a22 * b22;

    return [
      c00, c10, c20,
      c01, c11, c21,
      c02, c12, c22,
    ];
  },

  /**
   * Computes inverse of the given 3x3 matrix.
   * @param {number[]} a Column-major 3x3 matrix.
   * @returns {number[]} Inverse of the given matrix.
   */
  invert: function(a) {
    // first column of matrix a
    const a00 = a[0 * 3 + 0];
    const a10 = a[0 * 3 + 1];
    const a20 = a[0 * 3 + 2];
    // second column of matrix a
    const a01 = a[1 * 3 + 0];
    const a11 = a[1 * 3 + 1];
    const a21 = a[1 * 3 + 2];
    // third column of matrix a
    const a02 = a[2 * 3 + 0];
    const a12 = a[2 * 3 + 1];
    const a22 = a[2 * 3 + 2];

    const det = (a00 * a11 * a22 + a01 * a12 * a20 + a02 * a10 * a21)
             -(a02 * a11 * a20 + a01 * a10 * a22 + a00 * a12 * a21);

    const c00 = (a11 * a22 - a21 * a12) / det;
    const c01 = -(a10 * a22 - a20 * a12) / det;
    const c02 = (a20 * a11 - a10 * a21) / det;

    const c10 = -(a01 * a22 - a21 * a02) / det;
    const c11 = (a00 * a22 - a20 * a02) / det;
    const c12 = -(a00 * a21 - a20 * a01) / det;

    const c20 = (a01 * a12 - a11 * a02) / det;
    const c21 = -(a00 * a12 - a10 * a02) / det;
    const c22 = (a00 * a11 - a10 * a01) / det;

    return [
      c00, c01, c02,
      c10, c11, c12,
      c20, c21, c22,
    ];
  },

  /**
   * Returns a 3x3 identity matrix.
   * @returns {number[]} Identity matrix.
   */
  identity: function() {
    return [
      1, 0, 0,
      0, 1, 0,
      0, 0, 1,
    ];
  },

  /**
   * Returns a 3x3 fromTranslation transformation.
   * @param {number} tx Horizontal amount of fromTranslation.
   * @param {number} ty Vertical amount of fromTranslation.
   * @returns {number[]} Column-major 3x3 fromTranslation transformation.
   */
  fromTranslation: function(tx, ty) {
    return [
      1,  0,  0, // first column
      0,  1,  0, // second column
      tx, ty, 1, // third column
    ];
  },

  /**
   * Returns a 3x3 counterclockwise fromRotation transformation.
   * @param {number} angleInRadians Angle of fromRotation, in radians.
   * @returns {number[]} Column-major 3x3 counterclockwise transformation.
   */
  fromRotation: function(angleInRadians) {
    const c = Math.cos(angleInRadians);
    const s = Math.sin(angleInRadians);
    return [
       c,  s, 0, // first column
       -s,  c, 0, // second column
       0,  0, 1, // third column
    ];
  },

  /**
   * Returns a 3x3 fromScaling transformation.
   * @param {number} sx Amount of horizontal fromScaling.
   * @param {number} sy Amount of vertical fromScaling.
   * @returns {number[]} Column-major 3x3 fromScaling transformation.
   */
  fromScaling: function(sx, sy) {
    return [
      sx, 0, 0, // first column
      0, sy, 0, // second column
      0,  0, 1, // third column
    ];
  },

  /**
   * Returns a projection transformation which transforms coordinates from
   * screenspace to NDC. Transformation is defined by four characteristic points.
   * @param {number} left Leftmost visible screenspace x-coordinate.
   * @param {number} right Rightmost visible screenspace x-coordinate.
   * @param {number} bottom Lowest visible screenspace y-coordinate.
   * @param {number} top Highest visible screenspace y-coordinate.
   * @returns {number[]} Column-major 3x3 projection transformation.
   */
  orthographicProjection: function(left, right, bottom, top) {
    const w = right - left;
    const h = top - bottom;

    return [
      2 / w, 0, 0,
      0, 2 / h, 0,
      -(right + left) / w, -(top + bottom) / h, 1,
    ];
  },

  /**
   * Creates a new transformation where the first transform is fromTranslation.
   * @param {number[]} m Column-major 3x3 transformation.
   * @param {number} tx Horizontal amount of fromTranslation.
   * @param {number} ty Vertical amount of fromTranslation.
   * @returns {number[]} New column-major 3x3 transformation.
   */
  translate: function(m, tx, ty) {
    return mat3.multiply(m, mat3.fromTranslation(tx, ty));
  },

  /**
   * Creates a new transformation where the first transform is fromRotation.
   * @param {number[]} m Column-major 3x3 transformation.
   * @param {number} angle Angle of fromRotation, in radians.
   * @returns {number[]} New column-major 3x3 transformation.
   */
  rotate: function(m, angle) {
    return mat3.multiply(m, mat3.fromRotation(angle));
  },

  /**
   * Creates a new transformation where the first transform is fromScaling.
   * @param {number[]} m Column-major 3x3 transformation.
   * @param {number} sx Amount of horizontal fromScaling.
   * @param {number} sy Amount of vertical fromScaling.
   * @returns {number[]} New column-major 3x3 transformation.
   */
  scale: function(m, sx, sy) {
    return mat3.multiply(m, mat3.fromScaling(sx, sy));
  },
};


/**
 * Class representing a sequence of 3x3 column-major matrices which form a transformation.
 */
function MatrixSequence() {
  this.matrices = [mat3.identity()];
  this.transforms = [mat3.identity()];
}

/**
 * Resets the sequence into an identity matrix.
 */
MatrixSequence.prototype.clear = function() {
  this.matrices = [mat3.identity()];
  this.transforms = [mat3.identity()];
};

/**
 * Returns the i-th matrix of the i-th transformation.
 * @param {number} i Index of matrix.
 * @returns {number[]} A 3x3 column-major matrix.
 */
MatrixSequence.prototype.getMatrix = function(i) {
  if (i < 0 || i >= this.matrices.length) {
    return mat3.identity();
  }

  return this.matrices[i].slice();
};

/**
 * Sets i-th matrix of i-th transformation.
 * All transformations after i-th will be updated accordingly.
 * @param {number} i Index of matrix.
 * @param {number[]} m A 3x3 column-major matrix which represents the new transformation.
 */
MatrixSequence.prototype.setMatrix = function(i, m) {
  if (i < 0 || i >= this.matrices.length) {
    return;
  }

  m = m || mat3.identity();
  this.matrices[i] = m.slice();

  let before = mat3.identity();
  if (i > 0) {
    before = this.transforms[i - 1];
  }

  this.transforms[i] = mat3.multiply(m, before);
  for (let j = i + 1, n = this.matrices.length - 1; j <= n; j++) {
    this.transforms[j] = mat3.multiply(
      this.matrices[j],
      this.transforms[j - 1]
    );
  }
};

/**
 * Returns matrix of the most recent transformation.
 * @returns {number[]} Column-major 3x3 matrix which represents the most recent transformation.
 */
MatrixSequence.prototype.getCurrentMatrix = function() {
  return this.getMatrix(this.matrices.length - 1);
};

/**
 * Sets matrix of the most recent transformation.
 * @param {number[]} m A 3x3 column-major matrix which represents the new transformation.
 */
MatrixSequence.prototype.setCurrentMatrix = function(m) {
  this.setMatrix(this.matrices.length - 1, m);
};

/**
 * Returns the final transformation which this sequence of matrices
 * represents. First matrix is applied first, then the second one, and so on.
 * @returns {number[]} A 3x3 column-major transformation.
 */
MatrixSequence.prototype.getTransform = function() {
  return this.transforms[this.transforms.length - 1].slice();
};

/**
 * Transforms the given vertex with the final transformation, which is
 * same as applying all matrices of this sequence in order.
 * @param {number[]} vertex A 3D vector representing a 2D point in homogenous coordinates.
 * @returns {number[]} Original vertex transformed by this sequence.
 */
MatrixSequence.prototype.transform = function(vertex) {
  return mat3.multiplyVector(this.getTransform(), vertex);
};

/**
 * Removes i-th matrix from the sequence, and updates the final
 * transformation accordingly. If index is not given, most recent matrix is removed.
 * @param {number} [i] Index of matrix to pop.
 */
MatrixSequence.prototype.pop = function(i) {
  const stop = i || this.transforms.length - 2;
  for (let m = this.transforms.length - 1; m > stop; m--) {
    this.transforms.pop();
    this.matrices.pop();
  }
};

/**
 * Saves the current matrix, adds another matrix to the sequence, and sets it as the
 * current one.
 */
MatrixSequence.prototype.save = function() {
  this.matrices.push(mat3.identity());
  this.transforms.push(this.transforms[this.transforms.length - 1]);
};

/**
 * Translates the most recent matrix.
 * @param {number} tx Horizontal amount of fromTranslation.
 * @param {number} ty Vertical amount of fromTranslation.
 */
MatrixSequence.prototype.translate = function(tx, ty) {
  this.setCurrentMatrix(mat3.translate(this.getCurrentMatrix(), tx, ty));
};

/**
 * Translates the i-th matrix.
 * @param {number} i Index of matrix to translate.
 * @param {number} tx Horizontal amount of fromTranslation.
 * @param {number} ty Vertical amount of fromTranslation.
 */
MatrixSequence.prototype.translateAt = function(i, tx, ty) {
  this.setMatrix(i, mat3.translate(this.getMatrix(i), tx, ty));
};

/**
 * Rotates counterclockwise the most recent matrix.
 * @param {number} angle Angle of fromRotation, in radians.
 */
MatrixSequence.prototype.rotate = function(angle) {
  this.setCurrentMatrix(mat3.rotate(this.getCurrentMatrix(), angle));
};

/**
 * Rotates counterclockwise i-th matrix.
 * @param {number} i Index of matrix to rotate.
 * @param {number} angle Angle of fromRotation, in radians.
 */
MatrixSequence.prototype.rotateAt = function(i, angle) {
  this.setMatrix(i, mat3.rotate(this.getMatrix(i), angle));
};

/**
 * Scales the most recent matrix.
 * @param {number} sx Amount of horizontal fromScaling.
 * @param {number} sy Amount of vertical fromScaling.
 */
MatrixSequence.prototype.scale = function(sx, sy) {
  this.setCurrentMatrix(mat3.scale(this.getCurrentMatrix(), sx, sy));
};

/**
 * Scales i-th matrix.
 * @param {number} i Index of matrix to scale.
 * @param {number} sx Amount of horizontal fromScaling.
 * @param {number} sy Amount of vertical fromScaling.
 */
MatrixSequence.prototype.scaleAt = function(i, sx, sy) {
  this.setMatrix(i, mat3.scale(this.getMatrix(i), sx ,sy));
};
