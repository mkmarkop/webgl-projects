/*@#common-include#(math.js)*/
/*@#common-include#(entities.js)*/
/*@#include#(task.js)*/
/*@#include#(generator.js)*/

/**
 * Initializer function for question. Uses configuration file for
 * generating a new question based on parameters from the file.
 * @param {Object} questionConfig Configuration JSON object.
 */
// TODO would be nice if it was split into smaller methods
function questionInitialize(questionConfig) {
  /**
   * Data which will be saved to the database, contains question
   * specification, its correct answer and current user's input.
   * @external userData
   */
  userData = Generator.generate(questionConfig);
}


/**
 * Returns computed properties which can be used in frontend as echo directives.
 * @returns {Object} Computed properties.
 */
function getComputedProperties() {
    return {};
}


/**
 * Question evaluator for this question. Evaluates question if it isn't in the inital state.
 * @returns {Object} Correctness - percentage of points guessed correctly, and solved - boolean.
 */
// TODO split into smaller methods
function questionEvaluate() {
  const solved = userData.question.state.used.length > 0;
  let correct = 0;
  const n = House.boundingPoints.length;

  if (solved) {
    const correctMatrix = userData.evaluation.matrix;
    const userMatrix = userData.question.matrix;
    const threshold = userData.evaluation.epsilon;

    for (let i = 0; i < n; i++) {
      const housePoint = House.boundingPoints[i].slice();
      const correctPoint = mat3.multiplyVector(correctMatrix, housePoint);
      const userPoint = mat3.multiplyVector(userMatrix, housePoint);

      if (vec2.calculateDistance(userPoint, correctPoint) <= threshold) {
        correct += 1;
      }
    }
  }

  return {correctness: correct / n, solved: solved};
}


/**
 * Exports user state and question information which frontend basic question setup needs.
 * @returns {Object} Common question data - canvas size, cell size and goal matrix.
 */
function exportCommonState() {
    return userData.common;
}


/**
 * Exports question state depending on the type asked.
 * Can export either current user state, or the correct state.
 * @param {string} stateType Type of question state.
 * @returns {Object} Question state.
 */
function exportUserState(stateType) {
    if (stateType === "USER") {
        return userData.question.state;
    }
    if (stateType === "CORRECT") {
        return userData.question.correct;
    }

    return {};
}


/**
 * Exports empty state of user data question state.
 * @returns {Object} Initial question state, before any user input.
 */
function exportEmptyState() {
    return userData.init;
}


/**
 * Imports question state data which has been changed by the user, and saves it in database.
 * @param {Object} data Question state data; form must be the same as of the exported state.
 */
function importQuestionState(data) {
    userData.question.state = data.qs;
    userData.question.matrix = data.matrix;
}
