function randomInteger(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

const rotationStep = Math.PI / 4;

function randomRotation() {
  return randomInteger(0, 7) * rotationStep;
}


const Generator = {
  generate: function(questionConfig) {
    const canvasWidth = questionConfig.width;
    const canvasHeight = questionConfig.height;
    const cellsInColumn = questionConfig.cellsInColumn;
    const cellSize = canvasHeight / cellsInColumn;

    const ghouse = new House(cellSize);
    const rotationAmount = randomRotation();
    ghouse.matrixSeq.rotate(rotationAmount);
    ghouse.matrixSeq.save();

    return {
      common: {
        view: {
          width: canvasWidth,
          height: canvasHeight
        },
        model: {
          cellSize: cellSize,
          goalMatrix: mat3.fromRotation(rotationAmount)
        }
      },

      evaluation: {
        epsilon: questionConfig.epsilon,
        matrix: ghouse.matrix()
      },

      init: {
        available: [
          'rotate'
        ],
        used: []
      },


      question: {
        state: {
          available: [
            'rotate'
          ],
          used: []
        },

        matrix: mat3.identity(),
        
        correct: {
          available: [],
          used: [
            {
              name: 'rotate',
              data: {
                amount: rotationAmount
              }
            }
          ]
        }
      }
    };
  }
};