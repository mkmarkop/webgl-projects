/**
 * Class encapsulating mouse data. Contains information about current position
 * of the mouse, its previous position, last click offset and last click pivot.
 */
class Mouse {
  /**
   * Creates new Mouse class instance.
   */
  constructor() {
    this.currentPos = {x: 0, y: 0};
    this.previousPos = null;
    this.clickPivot = {x: 0, y: 0};
    this.clickOffset = {x: 0, y: 0};
  }

  /**
   * Resets all mouse variables to default values.
   */
  reset() {
    this.currentPos = {x: 0, y: 0};
    this.previousPos = null;
    this.clickPivot = {x: 0, y: 0};
    this.clickOffset = {x: 0, y: 0};
  }
}

/**
 * Class representing world model.
 */
class WorldModel {
  /**
   * Creates new world model based on the world parameters.
   * @param {Object} params World parameters.
   * @param {number} params.cellSize Size of a square cell of the world grid.
   * @param {number[]} params.goalMatrix Model matrix of the goal house, unscaled by cell size.
   */
  constructor(params) {
    const _grid = new Grid(params.cellSize);
    const _playerHouse = new House(params.cellSize);
    const _finishHouse = new House(params.cellSize);
    _finishHouse.matrixSeq.setCurrentMatrix(params.goalMatrix);

    /**
     * Returns world grid whose cell size determines size of all houses.
     * @returns {Grid} World grid object.
     */
    this.grid = function () {
      return _grid;
    };

    /**
     * Returns player house.
     * @returns {House} Player house object.
     */
    this.playerHouse = function () {
      return _playerHouse;
    };

    /**
     * Returns goal house, whose position the user must match.
     * @returns {House} Goal house object.
     */
    this.finishHouse = function () {
      return _finishHouse;
    };

    /**
     * Resets player house matrix to identity.
     */
    this.reset = function () {
      _playerHouse.matrixSeq.pop(1);
      _playerHouse.matrixSeq.setCurrentMatrix(mat3.identity());
    };
  }
}

/**
 * Class representing a view of the world model.
 */
class WorldView {
  /**
   * Creates a view of the given world model based on parameters.
   * @param {WorldModel} model World model that this view represents.
   * @param {HTMLCanvasElement} canvas Canvas element where the world has to be displayed.
   * @param {Object} params Canvas parameters.
   * @param {number} params.width Canvas width.
   * @param {number} params.height Canvas height.
   * @param {string} qid ID prefix for all question DOM objects.
   */
  constructor(model, canvas, params, qid) {
    let screen = {
      width: 0,
      height: 0,
    };

    const gl = canvas.getContext('webgl', { stencil: true });
    const shdManager = new ShaderManager(qid, gl);
    const gfxManager = new GFXManager(gl);
    const renderer = new Renderer(gl, gfxManager, shdManager);
    
    canvas.width = params.width;
    canvas.height = params.height;
    
    let canvasRect = canvas.getBoundingClientRect();
    
    this.mouseMoved = new AppEvent();
    this.mouseClicked = new AppEvent();
    this.mouseReleased = new AppEvent();
    this.canvasResized = new AppEvent();
    
    /**
     * Returns world position of the mouse, based on the mouse vent.
     * @param {MouseEvent} event Mouse event data.
     * @returns {Object} Mouse world position.
     */
    this.mousePosition = function(event) {
      return renderer.screenToWorld(this.screenCoords(event));
    };
    
    /**
     * Renders the world which this view represents.
     * @param {*} now Current timestamp.
     */
    this.render = function(now) {
      renderer.clear();
      renderer.drawGrid(model.grid());
      renderer.enableStencil();
      renderer.drawHouse(model.finishHouse());
      renderer.disableStencil();
      renderer.drawSilhouette(model.playerHouse());
      renderer.enableStencil();
      renderer.drawPartialHouse(model.playerHouse());
      renderer.disableStencil();
      window.requestAnimationFrame((now) => this.render(now));
    };
    
    /**
     * Updates canvas bounding rectangle and renderer.
     */
    this.resize = function() {
      canvasRect = canvas.getBoundingClientRect();
      screen = renderer.resize();
      this.canvasResized.notify(screen);
    };
    
    /**
     * Updates canvas bounding rectangle.
     */
    this.scroll = function() {
      canvasRect = canvas.getBoundingClientRect();
    };
    
    /**
     * Initializes world view and connects listeners with DOM elements' events.
     * @param {boolean} interactive True if user can control the world through this view.
     */
    this.show = function(interactive) {
      this.resize();
      if (interactive) {
        //window.onresize = this.resize.bind(this);
        window.onscroll = this.scroll.bind(this);
        canvas.onmousemove = this.onmousemove.bind(this);
        canvas.onmousedown = this.onmousedown.bind(this);
        canvas.onmouseout = this.onmouseout.bind(this);
        canvas.onmouseup = this.onmouseup.bind(this);
      }
      window.requestAnimationFrame((now) => this.render(now));
    };
    
    /**
     * Returns mouse position in screen coordinates.
     * @param {MouseEvent} event Mouse event.
     * @returns {Object} Mouse screen position.
     */
    this.screenCoords = function(event) {
      const x = event.clientX - canvasRect.left;
      const y = event.clientY - canvasRect.top;
      const screenX = 2 * x / screen.width - 1;
      const screenY = -2 * y / screen.height + 1;
      return {
        x: screenX,
        y: screenY,
      };
    };
    
    /**
     * Function called when mouse has moved over canvas element.
     * @param {MouseEvent} event Mouse event data.
     */
    this.onmousemove = function(event) {
      this.mouseMoved.notify(this.mousePosition(event));
    };
    
    /**
     * Function called when mouse has been pressed over canvas element.
     * @param {MouseEvent} event Mouse event data.
     */
    this.onmousedown = function(event) {
      this.mouseClicked.notify(this.mousePosition(event));
    };

    /**
     * Function called when mouse has just left canvas element.
     * @param {MouseEvent} event Mouse event data.
     */
    this.onmouseout = function(event) {
      this.mouseReleased.notify({});
    };
    
    /**
     * Function called when mouse has been released over canvas element.
     * @param {MouseEvent} event Mouse event data.
     */
    this.onmouseup = function(event) {
      this.mouseReleased.notify({});
    };
  }
}

/**
 * Class representing a world model controller.
 */
class WorldController {
  /**
   * Creates a world controller that listens world view and changes world model accordingly.
   * @param {WorldModel} model World model that this controller is responsible for.
   * @param {WorldView} view World view that notifies this controller of any user input.
   */
  constructor(model, view) {
    let _this = this;
    const mouse = new Mouse();
    const state = {
      intersecting: false,
      transforming: false,
    };
    let currentTransform = TransformFactory.create('default');
    
    /**
     * Resets world model, mouse and current state.
     */
    this.reset = function () {
      model.reset();
      mouse.reset();
      state.intersecting = false;
      state.transforming = false;
    };
    
    /**
     * Starts transformation at the current position.
     * @param {Object} pos Start of the transformation, in world coordinates.
     * @param {number} pos.x X-coordinate.
     * @param {number} pos.y Y-coordinate.
     */
    this.startTransform = function(pos) {
      if (!state.intersecting)
        return;
      state.transforming = true;
      mouse.currentPos = pos;
      mouse.clickPivot = pos;
      mouse.clickOffset = model.playerHouse().distanceVector(pos);
      currentTransform.start(mouse);
    };
    
    /**
     * Updates transformation at the current position.
     * @param {Object} pos Start of the transformation, in world coordinates.
     * @param {number} pos.x X-coordinate.
     * @param {number} pos.y Y-coordinate.
     */
    this.updateTransform = function(pos) {
      state.intersecting = model.playerHouse().contains(pos);
      if (!state.transforming)
        return;
      mouse.currentPos = pos;
      mouse.previousPos = mouse.previousPos || mouse.currentPos;
      currentTransform.update(model, mouse);
      mouse.previousPos = mouse.currentPos;
    };
    
    /**
     * Sets the current transformation to a specified amount.
     * @param {number} amount New transformation amount.
     */
    this.setTransform = function(amount) {
      currentTransform.setTo(model, amount);
    };
    
    /**
     * Ends the current transformation, and prepares for a new one.
     */
    this.endTransform = function () {
      if (!state.transforming)
        return;
      currentTransform.finish(model);
      state.transforming = false;
    };
    
    /**
     * Changes the type of transformation which is currently being applied to the
     * player house.
     * @param {Object} context Context that defines transformation and its type.
     */
    this.changeContext = function(context) {
      model.playerHouse().matrixSeq.save();
      currentTransform = context;
    };
    
    /**
     * Restores previous world state and changes the current transformation to
     * a given context.
     * @param {Object} context Context that defines transformation and its type.
     */
    this.restoreContext = function(context) {
      model.playerHouse().matrixSeq.pop();
      currentTransform = context;
    };

    view.canvasResized.subscribe(function (screen) {
      model.grid().updateOffset(screen);
    });
    view.mouseClicked.subscribe(function (pos) {
      _this.startTransform(pos);
    });
    view.mouseMoved.subscribe(function (pos) {
      _this.updateTransform(pos);
    });
    view.mouseReleased.subscribe(function () {
      _this.endTransform();
    });
  }
}

/**
 * Class representing application world. World consists of a grid, player's house
 * and goal house. Grid cell size determines the size of all houses. World is displayed
 * and can be controled through a canvas.
 */
class World {
   
  /**
   * Creates a new world based on the given parameters and elements.
   * @param {HTMLCanvasElement} canvas Canvas element with a WebGL context. Is used both as a world view and a controller.
   * @param {Object} data Object containing world data.
   * @param {Object} data.model World parameters.
   * @param {number} data.model.cellSize Size of a square cell of the world grid.
   * @param {number[]} data.model.goalMatrix Model matrix of the goal house, unscaled by cell size.
   * @param {Object} data.view Canvas data of the world view.
   * @param {number} data.view.width Canvas width.
   * @param {number} data.view.height Canvas height.
   * @param {string} qid ID prefix for all question DOM objects.
   */
  constructor(canvas, data, qid) {
    this.data = data;
    this.model = new WorldModel(data.model);
    this.view = new WorldView(this.model, canvas, data.view, qid);
    this.controller = new WorldController(this.model, this.view);
    
    /**
     * Resets world model and view through world controller.
     */
    this.reset = function() {
      this.controller.reset();
    };

    this.parameters = function() {
      return JSON.parse(JSON.stringify(this.data));
    };
    
    /**
     * Initializes world view.
     * @param {boolean} interactive True if user input can change the world.
     */
    this.show = function(interactive) {
      this.view.show(interactive);
    };
    
    /**
     * Changes current transformation context of the world.
     * @param {Object} context New world transformation context.
     */
    this.changeContext = function (context) {
      this.controller.changeContext(context);
    };

    /**
     * Sets the current transformation context to the specified amount.
     * @param {number} amount New world transformation amount.
     */
    this.setTransform = function(amount) {
      this.controller.setTransform(amount);
    };
    
    /**
     * Restores the current transformation context to the given previous one.
     * @param {Object} context New world transformation context.
     */
    this.restoreContext = function(context) {
      this.controller.restoreContext(context);
    };
    
    /**
     * Returns current matrix of the player's house that transforms its vertices to world coordinates.
     * @returns {number[]} Column-major 3x3 matrix.
     */
    this.matrix = function() {
      return this.model.playerHouse().matrix();
    };
  }
}

