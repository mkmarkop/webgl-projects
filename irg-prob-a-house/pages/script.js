/*@#common-include#(math.js)*/
/*@#common-include#(entities.js)*/

/*@#include#(utility.js)*/
/*@#include#(shaders.js)*/
/*@#include#(renderer.js)*/
/*@#include#(transforms.js)*/
/*@#include#(manager.js)*/
/*@#include#(world.js)*/
/*@#include#(application.js)*/

/**
 * ID of this specific question instance.
 * @external questionLocalID
 * @type {string}
 */
const application = new Application(questionLocalID);

const api = {
  /**
   * Initializes question with user state and common state.
   * @param {Object} cst Common state object.
   * @param {Object} userState User state object.
   */
  initQuestion: function (cst, userState) {
    /**
     * Flag which determines should the question be interactive or not.
     * @external readOnly
     * @type {boolean}
     */
    application.init(cst, userState, readOnly);
  },

  /**
   * Export function for taking most recently submitted user data.
   * @returns {Object} Current user input data.
   */
  takeState: function () {
    return application.exportState();
  },

  /**
   * Reset function for task redoing. Clears user data and resets application
   * to the original state, when the question was opened.
   * @param {Object} cst Common state object.
   * @param {Object} emptyState Empty state object.
   */
  resetState: function (cst, emptyState) {
    application.resetState(cst, emptyState);
  },

  /**
   * Reverts the question state to a state which was loaded when the page loaded.
   * @param {Object} cst Common state object.
   * @param {Object} loadedState Firstly loaded state object.
   */
  revertState: function (cst, loadedState) {
    application.revertState(cst, loadedState);
  }
};

// noinspection JSAnnotator
return api;