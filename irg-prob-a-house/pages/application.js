/**
 * Class representing the entire question application.
 */
class Application {
  /**
   * Creates a new application.
   * @param {string} questionID ID prefix for all question DOM objects.
   */
  constructor(questionID) {
    this.questionIdPrefix = questionID;
    this.world = undefined;
    this.manager = undefined;
    this.interactive = true;
  }

  /**
   * Initializes application.
   * @param {Object} cst Common state object that defines canvas and grid size and goal matrix.
   * @param {Object} userState User state object that defines available and used transformations.
   * @param {boolean} readOnly Determines should the question be noninteractive.
   */
  init(cst, userState, readOnly) {
    this.interactive = !readOnly;

    this.world = new World(byId(this.questionIdPrefix + '_canvas'), cst, this.questionIdPrefix);
    this.world.show(this.interactive);

    this.manager = new Manager({
      activeArea: byId(this.questionIdPrefix + '_dropZone'),
      availableArea: byId(this.questionIdPrefix + '_availableTransforms'),
      transformTemplate: byId(this.questionIdPrefix + '_transformTemplate')
    }, this.world);
    this.manager.show(userState, this.questionIdPrefix + '_transform-', this.interactive);
  }

  /**
   * Resets application to the original state.
   * @param {Object} cst Common state object that defines canvas and grid size and goal matrix.
   * @param {Object} emptyState Empty state object that defines only available transforms.
   */
  resetState(cst, emptyState) {
    this.manager.show(emptyState, this.questionIdPrefix + '_transform-', this.interactive);
  }

  /**
   * Reverts application to a previous state.
   * @param {Object} cst Common state object that defines canvas and grid size and goal matrix.
   * @param {Object} loadedState Loaded state object that defines available and used transformations.
   */
  revertState(cst, loadedState) {
    this.manager.show(loadedState, this.questionIdPrefix + '_transform-', this.interactive);
  }

  /**
   * Exports current user input.
   * @returns {Object} Current state of available and used transforms, and final matrix.
   */
  exportState() {
    return {
      qs: this.manager.toJSON(),
      matrix: this.world.matrix()
    };
  }
}
