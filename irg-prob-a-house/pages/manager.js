class TransformView {

  constructor(model, elements, data) {
    this.model = model;
    this.elements = elements;
    this.elements.div.id = this.model.id;
    this.data = data;

    this.amountInput = new AppEvent();
    this.stateChanged = new AppEvent();
  }

  connectEvents() {
    this.elements.div.addEventListener(
      'dragstart', e => {
        this.onDragStart(e);
      }, false
    );
    this.elements.input.addEventListener(
      'change', e => {
        this.onAmountInput(e);
      }, false
    );

    this.model.stateChanged.subscribe(() => {
      this.onStateChange();
    });
    this.model.amountChanged.subscribe(d => {
      this.onAmountChange(d);
    });
  }

  dom() {
    return this.elements.div;
  }

  isValidInput(value) {
    return value !== undefined && value === value;
  }

  onAmountInput(event) {
    const inputValue = this.parseInputData(event.target.value);
    if (this.isValidInput(inputValue)) {
      this.amountInput.notify(inputValue);
    }
  }

  onDragStart(event) {
    event.effectAllowed = 'move';
    event.dataTransfer.dropEffect = 'move';
    event.dataTransfer.setData('text/plain', event.target.id);
  }

  onAmountChange(amount) {
    this.elements.input.value = this.formatDisplayData(amount);
  }

  onStateChange() {
    this.show(true);
    this.stateChanged.notify({
      id: this.model.id,
      state: this.model.state,
    });
  }

  parseInputData(data) {
    return Number.parseFloat(data);
  }

  formatDisplayData(data) {
    return Number.parseFloat(data).toFixed(2);
  }

  show(interactive) {
    if (interactive) {
      this.connectEvents();
    }

    const elems = this.elements;
    elems.header.innerText = this.model.transform;
    elems.input.value = this.formatDisplayData(this.model.amount);
    elems.input.disabled = interactive ? (!this.model.active()) : true;
    elems.div.draggable = (!this.model.used());
  }
}

class TranslateView extends TransformView {

  constructor(model, elements, data) {
    super(model, elements, data);
  }

  parseInputData(data) {
    return Number.parseFloat(data) * this.data.model.cellSize;
  }

  formatDisplayData(data) {
    return (Number.parseFloat(data) / this.data.model.cellSize).toFixed(2);
  }
}

class RotateView extends TransformView {

  constructor(model, elements, data) {
    super(model, elements, data);
  }

  parseInputData(data) {
    const degrees = Number.parseFloat(data) % 360;
    return (degrees * Math.PI) / 180;
  }

  formatDisplayData(data) {
    const radians = Number.parseFloat(data);
    const degrees = (radians * 180) / Math.PI;
    const wrapped = (degrees - Math.floor(degrees / 360) * 360);
    return wrapped.toFixed(2);
  }
}

const TransformViewFactory = {

  create: function (transform, model, elements, data) {
    let type = Transforms.default;
    if (Transforms.hasOwnProperty(transform)) {
      type = Transforms[transform];
    }

    const params = data || {};

    switch (type) {
      case Transforms.translateX:
        return new TranslateView(model, elements, params);
      case Transforms.translateY:
        return new TranslateView(model, elements, params);
      case Transforms.rotate:
        return new RotateView(model, elements, params);
      default:
        return new TransformView(model, elements, params);
    }
  }
};


class ManagerModel {

  constructor() {
    this.defaultTransform = TransformFactory.create('default', 'default');
    this.transformElems = Object.create(null);
    this.activeTransforms = [];
    this.availableTransforms = [];
  }

  reset() {
    this.transformElems = Object.create(null);
    this.activeTransforms = [];
    this.availableTransforms = [];
  }

  fill(transforms, questionIdPrefix) {
    this.reset();
    this._fillAvailableTransforms(transforms, questionIdPrefix);
    this._fillUsedTransforms(transforms, questionIdPrefix);
  }

  _fillAvailableTransforms(transforms, questionIdPrefix) {
    const available = transforms.available;

    for (let i = 0, n = available.length; i < n; i++) {
      const id = questionIdPrefix + i;
      this.availableTransforms.push(TransformFactory.create(
        available[i], id, TransformState.available)
      );
      this.transformElems[id] = this.availableTransforms[
        this.availableTransforms.length - 1
      ];
    }
  }

  _fillUsedTransforms(transforms, questionIdPrefix) {
    const used = transforms.used;

    for (let i = 0, n = used.length; i < n; i++) {
      const id = questionIdPrefix + (this.availableTransforms.length + i);
      const state = (i === n - 1) ?
        TransformState.active : TransformState.used;
      this.activeTransforms.push(TransformFactory.create(
        used[i].name, id, state, used[i].data
      ));
      this.transformElems[id] = this.activeTransforms[
        this.activeTransforms.length - 1
      ];
    }
  }

  toJSON() {
    return {
      available: this._availableTransformsToJSON(),
      used: this._usedTransformsToJSON(),
    };
  }

  _availableTransformsToJSON() {
    const available = [];
    for (let i = 0, n = this.availableTransforms.length; i < n; i++) {
      available.push(this.availableTransforms[i].transform);
    }

    return available;
  }

  _usedTransformsToJSON() {
    const used = [];
    for (let i = 0, n = this.activeTransforms.length; i < n; i++) {
      used.push(this.activeTransforms[i].toJSON());
    }

    return used;
  }

  transform(key) {
    return this.transformElems[key];
  }

  getActive() {
    const n = this.activeTransforms.length;
    if (n === 0) {
      return this.defaultTransform;
    }

   return this.activeTransforms[n - 1];
  }

  setActive(id) {
    const transform = this.transform(id);
    if (transform === undefined) return false;
    if (!transform.available()) return false;

    const active = this.getActive();
    if (active !== undefined) {
      active.setUsed();
    }

    transform.setActive();
    this.activeTransforms.push(transform);
    this.availableTransforms = this.availableTransforms.filter(t => {
      return t.id !== transform.id;
    });

    return true;
  }

  setAvailable(id) {
    if (this.getActive() === undefined) return false;
    const transform = this.transform(id);
    if (transform === undefined) return false;
    if (!transform.active()) return false;

    transform.setAvailable();
    this.availableTransforms.push(this.activeTransforms.pop());
    const active = this.getActive();
    if (active !== undefined) {
      active.setActive();
    }

    return true;
  }
}


class ManagerView {

  constructor(model, elements, data) {
    this.model = model;
    this.elements = elements;
    this.data = data;
    this.transforms = Object.create(null);

    this.amountInput = new AppEvent();
    this.droppedActive = new AppEvent();
    this.droppedAvailable = new AppEvent();
    this.transformLoaded = new AppEvent();
  }

  connectEvents() {
    this.elements.activeArea.addEventListener(
      'drop', e => {
        e.preventDefault();
        const id = e.dataTransfer.getData('text');
        this.droppedActive.notify(id);
      }, false
    );
    this.elements.activeArea.addEventListener(
      'dragover', e => {
        this.onDragOver(e);
      }, false
    );

    this.elements.availableArea.addEventListener(
      'drop', e => {
        e.preventDefault();
        const id = e.dataTransfer.getData('text');
        this.droppedAvailable.notify(id);
      }, false
    );
    this.elements.availableArea.addEventListener(
      'dragover', e => {
        this.onDragOver(e);
      }, false
    );
  }

  onDragOver(event) {
    event.preventDefault();
    return false;
  }

  routeAmount(value) {
    this.amountInput.notify(value);
  }

  moveTransform(transform) {
    const elem = this.transforms[transform.id];
    if (transform.state === TransformState.available) {
      this.elements.availableArea.appendChild(elem.dom());
    } else if (transform.state === TransformState.active) {
      this.elements.activeArea.appendChild(elem.dom());
    }
  }

  reset() {
    Object.keys(this.transforms).forEach(key => {
      const elem = this.transforms[key].elements.div;
      elem.parentElement.removeChild(elem);
    });

    this.transforms = Object.create(null);
  }

  show(interactive) {
    if (interactive) {
      this.connectEvents();
    }

    this.reset();

    const availableFragment = document.createDocumentFragment();
    const usedFragment = document.createDocumentFragment();
    const template = this.elements.transformTemplate.content;

    for (let key in this.model.transformElems) {
      const transform = this.model.transformElems[key];
      const cloned = template.cloneNode(true); // deep copy
      const tview = TransformViewFactory.create(
        transform.transform, transform, {
          'div': byQuery(cloned, '.transform'),
          'header': byQuery(cloned, '.transform-name'),
          'input': byQuery(cloned, '.transform-amount'),
        }, this.data
      );
      this.transforms[key] = tview;
      if (interactive) {
        tview.amountInput.subscribe(a => {
          this.routeAmount(a);
        });
        tview.stateChanged.subscribe(t => {
          this.moveTransform(t);
        });
      }
      tview.show(interactive);

      if (transform.available()) {
        availableFragment.appendChild(cloned);
      } else {
        usedFragment.appendChild(cloned);
        this.transformLoaded.notify(transform.id);
      }
    }

    this.elements.availableArea.appendChild(availableFragment);
    this.elements.activeArea.appendChild(usedFragment);
  }
}

class ManagerController {

  constructor(model, view, world) {
    this.model = model;
    this.view = view;
    this.world = world;

    this.connectEvents();
  }

  connectEvents() {
    this.view.droppedActive.subscribe(id => {
      this.updateActive(id);
    });
    this.view.droppedAvailable.subscribe(id => {
      this.updateAvailable(id);
    });
    this.view.amountInput.subscribe(v => {
      this.amountInput(v);
    });
    this.view.transformLoaded.subscribe(id => {
      this.preloadTransformation(id);
    });
  }

  amountInput(value) {
    this.world.setTransform(value);
  }

  preloadTransformation(id) {
    const transform = this.model.transform(id);
    this.world.changeContext(transform);
    this.world.setTransform(transform.amount);
  }

  updateActive(id) {
    if (this.model.setActive(id)) {
      this.world.changeContext(
        this.model.getActive()
      );
    }
  }

  updateAvailable(id) {
    if (this.model.setAvailable(id)) {
      this.world.restoreContext(
        this.model.getActive()
      );
    }
  }
}

class Manager {

  constructor(elements, world) {
    this.world = world;
    this.model = new ManagerModel();
    this.view = new ManagerView(this.model, elements, this.world.parameters());
    this.controller = new ManagerController(
      this.model, this.view, this.world);
  }

  show(transforms, prefix, interactive) {
    this.world.reset();
    this.model.fill(transforms, prefix);
    this.view.show(interactive);
  }

  toJSON() {
    return this.model.toJSON();
  }
}
