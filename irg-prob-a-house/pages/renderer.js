'use strict';

/**
 * Enumeration that represents available graphical entities.
 */
const Resources = {
  GRID: 0,
  HOUSE: 1,
};

/**
 * Class that manages GL buffers of all available graphicals entities.
 */
class GFXManager {
  /**
   * Creates a new GFXManager.
   * @param {WebGLRenderingContext} gl WebGL canvas context.
   */
  constructor(gl) {
    this.library = Object.create(null);

    this.library[Resources.GRID] = Grid.createBuffers(gl);
    this.library[Resources.HOUSE] = House.createBuffers(gl, {
      wall:   [1, 0.878, 0.341],
      doors:  [0.525, 0.184, 0.015],
      windows: [0.764, 0.972, 0.952],
      roof:   [0.866, 0.149, 0.015],
    });
  }

  /**
   * Returns GL buffers of a graphical entity with the associated key.
   * @param {number} key Resource key of an available graphical entity.
   * @returns {Object} Object that contains GL buffers needed to draw the required entity.
   */
  resource(key) {
    return this.library[key];
  }
}

/**
 * Class that represents renderer. Renderer is responsible for dealing with WebGL context
 * and can draw all available graphical entities, if their associated buffers are supplied.
 */
class Renderer {
  /**
   * Creates a new renderer based on the given shader manager and graphics manager.
   * Shader manager must have all shaders required for drawing all the entities, just as
   * graphics manager must have all buffers required for drawing all the entities.
   * @param {WebGLRenderingContext} glContext WebGL canvas context.
   * @param {GFXManager} resources GL buffer manager.
   * @param {ShaderManager} shaders Shader manager.
   */
  constructor(glContext, resources, shaders) {
    this.gl = glContext;
    this.resources = resources;
    this.shaders = shaders;
    this.projectionMatrix = mat3.identity();
    this.unprojectMatrix = mat3.identity();
    this.screen = {
      width: 0,
      height: 0,
    };
  }

  /**
   * Converts a 2D point in screen space to world space.
   * @param {Object} pos 2D screen point. 
   * @param {number} pos.x X-coordinate in screen space.
   * @param {number} pos.y Y-coordinate in screen space.
   * @returns {Object} World position of the given screen point.
   */
  screenToWorld(pos) {
    const wrld = mat3.multiplyVector(this.unprojectMatrix, [pos.x, pos.y, 1]);
    return { x: wrld[0], y: wrld[1] };
  }

  /**
   * Updates WebGL viewport and projection matrix.
   * @returns {Object} Object containing canvas width and height, and center position.
   */
  resize() {
    const displayWidth = this.gl.canvas.clientWidth || this.gl.canvas.width;
    const displayHeight = this.gl.canvas.clientHeight || this.gl.canvas.height;

    this.gl.canvas.width = displayWidth;
    this.gl.canvas.height = displayHeight;

    this.gl.viewport(0, 0, displayWidth, displayHeight);

    this.projectionMatrix = mat3.orthographicProjection(
      -displayWidth / 2, displayWidth / 2,
      -displayHeight / 2, displayHeight / 2,
    );
    this.unprojectMatrix = mat3.invert(this.projectionMatrix);

    this.screen.width = displayWidth;
    this.screen.height = displayHeight;
    return {
      width: displayWidth,
      height: displayHeight,
      center: {
        x: displayWidth / 2,
        y: displayHeight / 2,
      },
    };
  }

  /**
   * Clears the viewport.
   */
  clear() {
    const gl = this.gl;
    gl.clearColor(0.0, 0.0, 0.0, 0.0);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.STENCIL_BUFFER_BIT);
  }

  /**
   * Enables the stencil buffer.
   */
  enableStencil() {
    const gl = this.gl;
    gl.enable(gl.STENCIL_TEST);
    // bitmask which is ANDed with the stencil value about to be written
    gl.stencilMask(0xFF); // each bit is written to stencil buffer as is
    // always pass stencil test, reference value 1, mask 1
    gl.stencilFunc(gl.ALWAYS, 1, 0xFF);
    // replace value in the stencil buffer with the reference value, whenever
    // stencil test succeeds (which is always)
    gl.stencilOp(gl.REPLACE, gl.REPLACE, gl.REPLACE);
  }

  /**
   * Disables the stencil buffer.
   */
  disableStencil() {
    const gl = this.gl;
    gl.disable(gl.STENCIL_TEST);
  }

  /**
   * Draws the given grid object.
   * @param {Grid} grid Grid object to draw.
   */
  drawGrid(grid) {
    const gl = this.gl;
    const gridShader = this.shaders.gridShader;
    const gfx = this.resources.resource(Resources.GRID);

    gl.useProgram(gridShader.program);
    gl.uniform2fv(gridShader.uniformLoc.cellSize, grid.cellSize);
    gl.uniform2fv(gridShader.uniformLoc.cellOffset, grid.offset);

    gl.bindBuffer(gl.ARRAY_BUFFER, gfx.vertex.buffer);
    gl.enableVertexAttribArray(gridShader.attribLoc.vertexPosition);
    gl.vertexAttribPointer(
      gridShader.attribLoc.vertexPosition,
      gfx.vertex.size,
      gfx.vertex.type,
      gfx.vertex.normalize,
      gfx.vertex.stride,
      gfx.vertex.offset
    );

    gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
  }

  /**
   * Draws a silhouette of the given house object.
   * @param {House} house House object to draw.
   */
  drawSilhouette(house) {
    const gl = this.gl;
    const gfx = this.resources.resource(Resources.HOUSE);
    const outlineShader = this.shaders.outlineShader;

    gl.useProgram(outlineShader.program);
    gl.uniformMatrix3fv(outlineShader.uniformLoc.projectionMatrix, false,
      this.projectionMatrix);
    gl.uniformMatrix3fv(outlineShader.uniformLoc.modelMatrix, false, house.matrix());

    gl.bindBuffer(gl.ARRAY_BUFFER, gfx.vertex.buffer);
    gl.enableVertexAttribArray(outlineShader.attribLoc.vertexPosition);
    gl.vertexAttribPointer(
      outlineShader.attribLoc.vertexPosition,
      gfx.vertex.size,
      gfx.vertex.type,
      gfx.vertex.normalize,
      gfx.vertex.stride,
      gfx.vertex.offset,
    );

    gl.drawArrays(gl.TRIANGLES, 0, 18 * 3);
  }

  /**
   * Draws the given house object.
   * @param {House} house House object to draw.
   * @param {boolean} highlight Should the house be highlighted or not.
   */
  drawHouse(house, highlight = false) {
    const gl = this.gl;
    const gfx = this.resources.resource(Resources.HOUSE);
    const pixelShader = this.shaders.pixelShader;
    highlight = highlight || false;

    gl.useProgram(pixelShader.program);

    gl.uniform1f(pixelShader.uniformLoc.highlight,
      highlight === true ? 1 : 0);
    gl.uniformMatrix3fv(pixelShader.uniformLoc.projectionMatrix, false,
      this.projectionMatrix);

    gl.uniformMatrix3fv(pixelShader.uniformLoc.modelMatrix, false, house.matrix());
    gl.bindBuffer(gl.ARRAY_BUFFER, gfx.color.buffer);
    gl.enableVertexAttribArray(pixelShader.attribLoc.vertexColor);
    gl.vertexAttribPointer(
      pixelShader.attribLoc.vertexColor,
      gfx.color.size,
      gfx.color.type,
      gfx.color.normalize,
      gfx.color.stride,
      gfx.color.offset,
    );
    gl.bindBuffer(gl.ARRAY_BUFFER, gfx.vertex.buffer);
    gl.enableVertexAttribArray(pixelShader.attribLoc.vertexPosition);
    gl.vertexAttribPointer(
      pixelShader.attribLoc.vertexPosition,
      gfx.vertex.size,
      gfx.vertex.type,
      gfx.vertex.normalize,
      gfx.vertex.stride,
      gfx.vertex.offset,
    );
    gl.drawArrays(gl.TRIANGLES, 0, 18 * 3);
  }

  /**
   * Based on current stencil buffer value, draws house, but its overlapping
   * portion will be highlighted.
   * @param {House} house House object to draw.
   */
  drawPartialHouse(house) {
    const gl = this.gl;
    this._drawOverlappingPartOfHouse(gl, house);
    this._drawNonOverlappingPartOfHouse(gl, house);
  }

  _drawOverlappingPartOfHouse(gl, house) {
    // If stencil buffer is not equal to 0, keep it and AND it with 0xFF.
    gl.stencilFunc(gl.NOTEQUAL, 0, 0xFF);
    // Stencil test fail, z-test fail, z-test pass.
    gl.stencilOp(gl.KEEP, gl.KEEP, gl.KEEP);

    gl.enable(gl.BLEND);
    gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
    // Draw overlapping portion of the house.
    this.drawHouse(house, true);
  }

  _drawNonOverlappingPartOfHouse(gl, house) {
    // If stencil buffer is empty, house can be draw there.
    gl.stencilFunc(gl.EQUAL, 0, 0xFF);
    gl.disable(gl.BLEND);
    // Draw portion of house that doesn't overlap with existing house.
    this.drawHouse(house);
    gl.disable(gl.STENCIL_TEST);
  }
}
