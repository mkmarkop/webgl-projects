"use strict";

/**
 * Grabs DOM element by its ID.
 * @param {string} id ID of a DOM element.
 * @returns {Element} DOM element with the matching ID.
 */
function byId(id) {
  return document.getElementById(id);
}

/**
 * Grabs the first descendant element of the given DOM element
 * which matches the given group of selectors.
 * @param {Element} obj DOM element whose descendants will be queried
 * @param {string} query Query which a descendant must match.
 * @returns {Element} First descendant that matches the given query.
 */
function byQuery(obj, query) {
  return obj.querySelector(query);
}

/**
 * Compiles given source into either vertex or fragment shader, depending on the type.
 * @param {WebGLRenderingContext} gl WebGL canvas context.
 * @param {number} type Type of shader to compile, can be either gl.VERTEX_SHADER or gl.FRAGMENT_SHADER.
 * @param {string} source Shader source code.
 * @returns {WebGLShader} Compiled shader of the given type.
 */
function createShader(gl, type, source) {
  const shader = gl.createShader(type);
  gl.shaderSource(shader, source);
  gl.compileShader(shader);
  const success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
  if (success) {
    return shader;
  }

  console.log(type);
  console.log(gl.getShaderInfoLog(shader));
  gl.deleteShader(shader);
}

/**
 * Links existing vertex and fragment shaders into a program, and returns it.
 * @param {WebGLRenderingContext} gl WebGL canvas context.
 * @param {WebGLShader} vertexShader Compiled vertex shader.
 * @param {WebGLShader} fragmentShader Compiled fragment shader.
 * @returns {WebGLProgram} Program with linked vertex and fragment shaders.
 */
function createProgram(gl, vertexShader, fragmentShader) {
  const program = gl.createProgram();
  gl.attachShader(program, vertexShader);
  gl.attachShader(program, fragmentShader);
  gl.linkProgram(program);
  const success = gl.getProgramParameter(program, gl.LINK_STATUS);
  if (success) {
    return program;
  }

  console.log(gl.getProgramInfoLog(program));
  gl.deleteProgram(program);
}

/**
 * Compiles vertex and fragment shaders from source, links them, creates a program
 * and returns it.
 * @param {WebGLRenderingContext} gl WebGL canvas context.
 * @param {string} vertexShaderSource Source code of the vertex shader.
 * @param {string} fragmentShaderSource Source code of the fragment shader.
 * @returns {WebGLProgram} Program with linked vertex and fragment shaders.
 */
function compileProgram(gl, vertexShaderSource, fragmentShaderSource) {
  const vertexShader = createShader(gl, gl.VERTEX_SHADER, vertexShaderSource);
  const fragmentShader = createShader(gl, gl.FRAGMENT_SHADER, fragmentShaderSource);
  // link the two shaders into a program
  return createProgram(gl, vertexShader, fragmentShader);
}
