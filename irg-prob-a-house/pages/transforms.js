/**
 * Enumeration that contains all possible states of a transform.
 * Transforms can be available, used or active. Only active transforms
 * can change the world state.
 * @type {Object}
 */
const TransformState = {
  available: 0,
  active: 1,
  used: 2,
};

/**
 * Enumeration that contains all possible types of transformations.
 * @type {Object}
 */
const Transforms = {
  default: 0,
  translateX: 1,
  translateY: 2,
  scaleX: 3,
  scaleY: 4,
  rotate: 5,
};

/**
 * Class that represents a transform that doesn't change anything.
 * All other transforms are inherited from it.
 */
class NullTransform {
  /**
   * Creates a new NullTransform instance.
   * @param {string} id ID of this transform.
   * @param {TransformState} state Current state of this transform.
   * @param {Object} data Object that contains state data.
   * @param {number} data.amount Current transform amount.
   * @param {Object} data.firstPivot First (x,y) start point of the transform.
   * @param {number[]} data.lastScaling Last x- and y-fromScaling when the transform was last time applied.
   * @param {number} defaultAmount Transform amount that won't change world state.
   */
  constructor(id, state = TransformState.available, data = {}, defaultAmount = 0) {
    this.id = id;
    this.state = state;
    this.stateChanged = new AppEvent();
    this.amountChanged = new AppEvent();
    this.defaultAmount = defaultAmount;
    this.amount = data.amount || this.defaultAmount;
    this.reset(data);
  }

  /**
   * Resets transform state data to given parameters.
   * @param {Object} params Object that contains state data.
   * @param {number} params.amount Current transform amount.
   * @param {Object} params.firstPivot First (x,y) start point of the transform.
   * @param {number[]} params.lastScaling Last x- and y-fromScaling when the transform was last time applied.
   */
  reset(params) {
    this.lastScaling = params.lastScaling || [1, 1];
    this.lastMatrix = mat3.identity();
    this.lastOffset = null;
    this.firstPivot = params.firstPivot || null;
  }

  /**
   * Returns object representing current transform state.
   * Transform state can be reconstructed from this data.
   * @returns {Object} Object representing transform's current state.
   */
  toJSON() {
    return {
      name: this.transform,
      data: {
        amount: this.amount,
        firstPivot: this.firstPivot,
        lastScaling: this.lastScaling,
      },
    };
  }

  /**
   * Stars a new transformation of this type.
   * @param {Mouse} mouse Mouse object.
   */
  start(mouse) {
    this.firstPivot = this.firstPivot || mouse.currentPos;
    this.lastOffset = {
      x: this.firstPivot.x * this.lastScaling[0] - mouse.clickPivot.x,
      y: this.firstPivot.y * this.lastScaling[1] - mouse.clickPivot.y,
    };
  }

  /**
   * Calculates new transformation amount based on the current mouse data.
   * @param {WorldModel} model World model.
   * @param {Mouse} mouse Current mouse data.
   */
  calculate(model, mouse) {}

  /**
   * Updates the value of current transformation based on the current mouse data.
   * @param {WorldModel} model World model.
   * @param {Mouse} mouse Current mouse data.
   */
  update(model, mouse) {
    this.calculate(model, mouse);
    this.amountChanged.notify(this.amount);
  }

  /**
   * Sets the value of current transformation.
   * @param {WorldModel} model World model.
   * @param {number} amount New transform amount.
   */
  set(model, amount) {}

  /**
   * Sets the value of current transformation and finishes
   * current transformation.
   * @param {WorldModel} model World model.
   * @param {number} amount New transform amount.
   */
  setTo(model, amount) {
    this.set(model, amount);
    this.finish(model);
  }

  /**
   * Finishes current transformation.
   * @param {WorldModel} model World model.
   */
  finish(model) {
    this.lastMatrix = model.playerHouse().matrixSeq.getCurrentMatrix();
    this.lastScaling = mat3.multiply(this.lastMatrix, [1, 1, 1]).slice(0, 2);
  }

  /**
   * Returns type of this transform that is unique for every
   * class of transforms.
   * @returns {string} Type of this transform.
   */
  get transform() {
    return 'default';
  }

  /**
   * Returns true if this transform is active.
   * @returns {boolean} True if this transform is active.
   */
  active() {
    return this.state === TransformState.active;
  }

  /**
   * Return true if this transform is available.
   * @returns {boolean} True if this transform is available.
   */
  available() {
    return this.state === TransformState.available;
  }

  /**
   * Returns true if this transform is used.
   * @returns {boolean} True if this transform is used.
   */
  used() {
    return this.state === TransformState.used;
  }

 /**
   * Sets transform's state to active and notifies all subscribers.
   */
  setActive() {
    this.state = TransformState.active;
    this.stateChanged.notify({});
  }

  /**
   * Sets transform's state to available and notifies all subscribers.
   */
  setAvailable() {
    this.state = TransformState.available;
    this.amount = this.defaultAmount;
    this.reset({});
    this.stateChanged.notify({});
  }

  /**
   * Sets transform's state to used and notifies all subscribers.
   */
  setUsed() {
    this.state = TransformState.used;
    this.stateChanged.notify({});
  }
}

/**
 * Class that represents horizontal fromTranslation transformation.
 */
class XTranslateTransform extends NullTransform {
  /**
   * Creates a new XTranslateTransform instance.
   * @param {string} id ID of this transform.
   * @param {TransformState} state Current state of this transform.
   * @param {Object} data Object that contains state data.
   * @param {number} data.amount Current transform amount.
   * @param {Object} data.firstPivot First (x,y) start point of the transform.
   * @param {number[]} data.lastScaling Last x- and y-fromScaling when the transform was last time applied.
   */
  constructor(id, state, data) {
    super(id, state, data);
  }

  calculate(model, mouse) {
    const nextX = mouse.currentPos.x + mouse.clickOffset.x;
    const house = model.playerHouse();
    const amount = nextX - house.position().x;
    house.matrixSeq.translate(amount, 0);
    this.amount += amount;
  }

  set(model, amount) {
    this.amount = amount;
    model.playerHouse().matrixSeq.setCurrentMatrix(
      mat3.fromTranslation(this.amount, 0)
    );
  }

  get transform() {
    return 'translateX';
  }
}

/**
 * Class that represents vertical fromTranslation transformation.
 */
class YTranslateTransform extends NullTransform {
  /**
   * Creates a new YTranslateTransform instance.
   * @param {string} id ID of this transform.
   * @param {TransformState} state Current state of this transform.
   * @param {Object} data Object that contains state data.
   * @param {number} data.amount Current transform amount.
   * @param {Object} data.firstPivot First (x,y) start point of the transform.
   * @param {number[]} data.lastScaling Last x- and y-fromScaling when the transform was last time applied.
   */
  constructor(id, state, data) {
    super(id, state, data);
  }

  calculate(model, mouse) {
    const nextY = mouse.currentPos.y + mouse.clickOffset.y;
    const house = model.playerHouse();
    const amount = nextY - house.position().y;
    house.matrixSeq.translate(0, amount);
    this.amount += amount;
  }

  set(model, amount) {
    this.amount = amount;
    model.playerHouse().matrixSeq.setCurrentMatrix(
      mat3.fromTranslation(0, this.amount)
    );
  }

  get transform() {
    return 'translateY';
  }
}

/**
 * Class that represents fromRotation transformation.
 */
class RotateTransform extends NullTransform {
  /**
   * Creates a new RotateTransform instance.
   * @param {string} id ID of this transform.
   * @param {TransformState} state Current state of this transform.
   * @param {Object} data Object that contains state data.
   * @param {number} data.amount Current transform amount.
   * @param {Object} data.firstPivot First (x,y) start point of the transform.
   * @param {number[]} data.lastScaling Last x- and y-fromScaling when the transform was last time applied.
   */
  constructor(id, state, data) {
    super(id, state, data);
  }

  calculate(model, mouse) {
    const angle = 2 * (mouse.currentPos.x - mouse.previousPos.x)
                      / model.grid().size().width;
    model.playerHouse().matrixSeq.rotate(angle);
    this.amount += angle;
  }

  set(model, amount) {
    this.amount = amount;
    model.playerHouse().matrixSeq.setCurrentMatrix(
      mat3.fromRotation(this.amount)
    );
  }

  get transform() {
    return 'rotate';
  }
}

/**
 * Class that represents horizontal fromScaling transformation.
 */
class XScaleTransform extends NullTransform {
  /**
   * Creates a new XScaleTransform instance.
   * @param {string} id ID of this transform.
   * @param {TransformState} state Current state of this transform.
   * @param {Object} data Object that contains state data.
   * @param {number} data.amount Current transform amount.
   * @param {Object} data.firstPivot First (x,y) start point of the transform.
   * @param {number[]} data.lastScaling Last x- and y-fromScaling when the transform was last time applied.
   */
  constructor(id, state, data) {
    super(id, state, data, 1);
  }

  calculate(model, mouse) {
    this.amount = (mouse.currentPos.x + this.lastOffset.x)
                  / this.firstPivot.x;
    model.playerHouse().matrixSeq.setCurrentMatrix(
      mat3.fromScaling(this.amount, 1)
    );
  }

  set(model, amount) {
    this.amount = amount;
    model.playerHouse().matrixSeq.setCurrentMatrix(
      mat3.fromScaling(this.amount, 1)
    );
  }

  get transform() {
    return 'scaleX';
  }
}

/**
 * Class that represents vertical fromTranslation transformation.
 */
class YScaleTransform extends NullTransform {
  /**
   * Creates a new YScaleTransform instance.
   * @param {string} id ID of this transform.
   * @param {TransformState} state Current state of this transform.
   * @param {Object} data Object that contains state data.
   * @param {number} data.amount Current transform amount.
   * @param {Object} data.firstPivot First (x,y) start point of the transform.
   * @param {number[]} data.lastScaling Last x- and y-fromScaling when the transform was last time applied.
   */
  constructor(id, state, data) {
    super(id, state, data, 1);
  }

  calculate(model, mouse) {
    this.amount = (mouse.currentPos.y + this.lastOffset.y)
                  / this.firstPivot.y;
    model.playerHouse().matrixSeq.setCurrentMatrix(
      mat3.fromScaling(1, this.amount)
    );
  }

  set(model, amount) {
    this.amount = amount;
    model.playerHouse().matrixSeq.setCurrentMatrix(
      mat3.fromScaling(1, this.amount)
    );
  }

  get transform() {
    return 'scaleY';
  }
}

/**
 * Singleton class that creates world transforms.
 */
const TransformFactory = {
  /**
   * Creates a new transform based on the given parameters and its type.
   * If no such transform type exists, returns the default (identity) transform.
   * @param {string} transform Type of transform to create.
   * @param {string} id ID of the to-be-created transform.
   * @param {TransformState} state State of the transform.
   * @param {Object} data Object that contains state data.
   * @param {number} data.amount Current transform amount.
   * @param {Object} data.firstPivot First (x,y) start point of the transform.
   * @param {number[]} data.lastScaling Last x- and y-fromScaling when the transform was last time applied.
   * @returns {NullTransform} New instance of the requested world transform.
   */
  create: function(transform, id, state = TransformState.available, data = {}) {
    let type = Transforms.default;
    if (Transforms.hasOwnProperty(transform)) {
      type = Transforms[transform];
    }

    switch (type) {
      case Transforms.translateX:
        return new XTranslateTransform(id, state, data);
      case Transforms.translateY:
        return new YTranslateTransform(id, state, data);
      case Transforms.scaleX:
        return new XScaleTransform(id, state, data);
      case Transforms.scaleY:
        return new YScaleTransform(id, state, data);
      case Transforms.rotate:
        return new RotateTransform(id, state, data);
      default:
        return new NullTransform(id, state, data);
    }
  }
};