/**
 * Loads pixel shader which is used for drawing a house object.
 * @param {string} qid ID prefix for all question DOM objects
 * @param {WebGLRenderingContext} gl WebGL canvas context.
 * @returns {Object} Compiled shader together with its uniform and attribute locations.
 */
function loadPixelShader(qid, gl) {
  const vsSource = byId(qid + '_vertex-shader').text;
  const fsSource = byId(qid + '_fragment-shader').text;
  const program = compileProgram(gl, vsSource, fsSource);

  return {
    program: program,

    uniformLoc: {
      modelMatrix: gl.getUniformLocation(program, 'u_modelMatrix'),
      projectionMatrix: gl.getUniformLocation(program, 'u_projectionMatrix'),
      highlight: gl.getUniformLocation(program, 'u_highlight'),
    },

    attribLoc: {
      vertexPosition: gl.getAttribLocation(program, 'a_position'),
      vertexColor: gl.getAttribLocation(program, 'a_color'),
    },
  };
}

/**
 * Loads outline shader which is used for drawing the outline of a house object.
 * @param {string} qid ID prefix for all question DOM objects
 * @param {WebGLRenderingContext} gl WebGL canvas context.
 * @returns {Object} Compiled shader together with its uniform and attribute locations.
 */
function loadOutlineShader(qid, gl) {
  const vsSource = byId(qid + '_outline-vertex-shader').text;
  const fsSource = byId(qid + '_outline-fragment-shader').text;
  const program = compileProgram(gl, vsSource, fsSource);

  return {
    program: program,

    uniformLoc: {
      modelMatrix: gl.getUniformLocation(program, 'u_modelMatrix'),
      projectionMatrix: gl.getUniformLocation(program, 'u_projectionMatrix'),
      offset: gl.getUniformLocation(program, 'u_offset'),
      color: gl.getUniformLocation(program, 'u_color'),
    },

    attribLoc: {
      vertexPosition: gl.getAttribLocation(program, 'a_position'),
    },
  };
}

/**
 * Loads grid shader which is used for drawing the grid.
 * @param {string} qid ID prefix for all question DOM objects
 * @param {WebGLRenderingContext} gl WebGL canvas context.
 * @returns {Object} Compiled shader together with its uniform and attribute locations.
 */
function loadGridShader(qid, gl) {
  const vsSource = byId(qid + '_grid-vertex-shader').text;
  const fsSource = byId(qid + '_grid-fragment-shader').text;
  const program = compileProgram(gl, vsSource, fsSource);

  return {
    program: program,

    uniformLoc: {
      cellOffset: gl.getUniformLocation(program, 'u_offset'),
      cellSize: gl.getUniformLocation(program, 'u_cellSize'),
    },

    attribLoc: {
      vertexPosition: gl.getAttribLocation(program, 'a_position'),
    },
  };
}

/**
 * Class that contains all compiled shader programs for the application.
 */
class ShaderManager {
  /**
   * 
   * @param {string} qid ID prefix for all question DOM objects
   * @param {WebGLRenderingContext} gl WebGL canvas context.
   */
  constructor(qid, gl) {
    this.gridShader = loadGridShader(qid, gl);
    this.pixelShader = loadPixelShader(qid, gl);
    this.outlineShader = loadOutlineShader(qid, gl);
  }
}
